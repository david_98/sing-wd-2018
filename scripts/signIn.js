function sISingIn(){
    (function(d){
        var username = d.getElementById("username").value;
        var password = d.getElementById("password").value;

        if(JSON.parse(window.localStorage.getItem("registrovaniKorisnici")) == null){
            var registrovaniKorisnici = [];
            var korisnik = {
                "korisnickoIme": username,
                "sifra": password,
                "uloga": "korisnik",
                "kupljeneKnjige": [],
                "korpa": []
            };
            registrovaniKorisnici.push(korisnik);
            window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
            window.localStorage.setItem("trenutniKorisnik", JSON.stringify(korisnik));
            window.location.replace("../templates/main.html");
        } else {
            var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
            var upisi = true;
            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == username){
                    upisi = false;
                };
            };
            if(upisi){
                var korisnik = {
                    "korisnickoIme": username,
                    "sifra": password,
                    "uloga": "korisnik",
                    "kupljeneKnjige": [],
                    "korpa": []
                };
                registrovaniKorisnici.push(korisnik);
                window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
                window.localStorage.setItem("trenutniKorisnik", JSON.stringify(korisnik));
                window.location.replace("../templates/main.html");
            } else {
                window.alert("Korisnicko ime je zauzeto. Pokusajte ponovo.");
            };
        };
    })(document)
};

function sIPocetna(){
    (function(d){
        window.location.replace("../templates/pocetna.html");
    })(document);
};