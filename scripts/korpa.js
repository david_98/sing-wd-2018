(function(d){
    var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
    var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));

    var divZaPlacanje = d.getElementById("divZaPlacanje");
    if(trenutniKorisnik.korpa.length == 0){
        divZaPlacanje.setAttribute("style", "display:none;");
    };

    var parent = d.getElementById("divBoxBooks");
    while(parent.hasChildNodes()){
        parent.removeChild(parent.firstChild);
    };
    var cena = 0;
    for(let i = 0; i < trenutniKorisnik.korpa.length; i++){
        var div = document.createElement("div");
        div.classList.add("book");
        var image = document.createElement("img");
        image.src = trenutniKorisnik.korpa[i].slika;
        image.alt = trenutniKorisnik.korpa[i].naziv;
        var h3 = document.createElement("h3");
        h3.classList.add("naslovKnjige");
        h3.innerHTML = trenutniKorisnik.korpa[i].naziv;
        var h4 = document.createElement("h4");
        h4.classList.add("naslovKnjige");
        h4.id = "cenaKnjige";
        h4.innerHTML = "Cena: "+ trenutniKorisnik.korpa[i].cena + " dinara";
        var a = document.createElement("a");
        a.innerHTML = "izbaci iz korpe";
        a.onclick = function(){
            trenutniKorisnik.korpa.splice(i, 1);
            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == trenutniKorisnik.korisnickoIme){
                    korisnik.korpa = trenutniKorisnik.korpa;
                };
            };
            window.localStorage.setItem("trenutniKorisnik", JSON.stringify(trenutniKorisnik));
            window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
            window.location.reload();
        };
        div.appendChild(image);
        div.appendChild(h3);
        div.appendChild(h4);
        div.appendChild(a);
        parent.appendChild(div);
        cena = cena + trenutniKorisnik.korpa[i].cena;
    };
    var h3Cena = d.getElementById("h3Cena");
    h3Cena.innerHTML = "Ukupno: " + cena + " dinara";
})(document);

function kPlati(){
    (function(d){
        if (confirm("Zelite da kupite knjige?")) {
            var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
            var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));
            var knjige = JSON.parse(window.localStorage.getItem("knjige"));
            var registrovaniKorisnik = undefined;

            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == trenutniKorisnik.korisnickoIme){
                    registrovaniKorisnik = korisnik;
                };
            };
            
            for(knjigaUKorpi of trenutniKorisnik.korpa){
                for(knjiga of knjige){
                    if(knjigaUKorpi.naziv == knjiga.naziv && knjigaUKorpi.id == knjiga.id){
                        knjiga.kolicina = knjiga.kolicina - 1;
                        trenutniKorisnik.kupljeneKnjige.push(knjiga);
                        registrovaniKorisnik.kupljeneKnjige.push(knjiga);
                    };
                };
            };

            trenutniKorisnik.korpa = [];
            registrovaniKorisnik.korpa = [];

            window.localStorage.setItem("knjige", JSON.stringify(knjige));
            window.localStorage.setItem("trenutniKorisnik", JSON.stringify(trenutniKorisnik));
            window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
            window.location.replace("../templates/istorijaKupovine.html");
        } else {
        };
    })(document);
};

function kPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};

function kIstorijaKupovine(){
    (function(d){
        window.location.replace("../templates/istorijaKupovine.html");
    })(document);
};