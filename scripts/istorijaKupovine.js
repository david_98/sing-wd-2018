(function(d){
    var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));;
    var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));

    var parent = d.getElementById("divBoxBooks");
    while(parent.hasChildNodes()){
        parent.removeChild(parent.firstChild);
    };
    for(let i = 0; i < trenutniKorisnik.kupljeneKnjige.length; i++){
        var div = document.createElement("div");
        div.classList.add("book");
        var image = document.createElement("img");
        image.src = trenutniKorisnik.kupljeneKnjige[i].slika;
        image.alt = trenutniKorisnik.kupljeneKnjige[i].naziv;
        var h3 = document.createElement("h3");
        h3.classList.add("naslovKnjige");
        h3.innerHTML = trenutniKorisnik.kupljeneKnjige[i].naziv;
        div.appendChild(image);
        div.appendChild(h3);
        parent.appendChild(div);
    };
})(document);


function iKKorpa(){
    (function(d){
        window.location.replace("../templates/korpa.html");
    })(document);
};

function iKPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};