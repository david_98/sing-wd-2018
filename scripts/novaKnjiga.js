(function(d){
    var knjige = JSON.parse(window.localStorage.getItem("knjige"));
    var autori = JSON.parse(window.localStorage.getItem("autori"));
    var zanrovi = JSON.parse(window.localStorage.getItem("zanrovi"));

    var selectAutori = d.getElementById("selectChooseAnAuthor");
    var selectZanr = d.getElementById("selectZanr");
    for(autor of autori){
        var optionAutor = d.createElement("option");
        optionAutor.innerHTML = autor.ime + " " + autor.prezime;
        optionAutor.value = autor.ime + " " + autor.prezime;
        selectAutori.appendChild(optionAutor);
    };
    for(zanr of zanrovi){
        var optionZanr = d.createElement("option");
        optionZanr.innerHTML = zanr;
        optionZanr.value = zanr;
        selectZanr.appendChild(optionZanr);
    };

    var id = d.getElementById("id");
    id.value = knjige[knjige.length-1].id + 1;
})(document);

var putanjaSlike = undefined;

function nKDodaj(){
    (function(d){
        if(putanjaSlike == undefined){
            window.alert("Izaberite sliku knjige.");
            return;
        };
        var naziv = d.getElementById("naziv").value;
        var id = d.getElementById("id").value;
        var brStrana = d.getElementById("brStrana").value;
        var cena = d.getElementById("cena").value;
        var brPrimeraka = d.getElementById("brPrimeraka").value;
        var godIzdavanja = d.getElementById("godIzdavanja").value;
        var zanr = d.getElementById("selectZanr").selectedOptions[0].value;
        var selectChooseAnAuthor = d.getElementById("selectChooseAnAuthor").selectedOptions[0].value;
        selectChooseAnAuthor = 
        selectChooseAnAuthor.split(" ");
        selectChooseAnAuthor = {
            "ime": selectChooseAnAuthor[0],
            "prezime": selectChooseAnAuthor[1]
        };
        var prica = d.getElementById("prica").value;
        var knjige = JSON.parse(window.localStorage.getItem("knjige"));
        var upisi = true;
        for(knjiga of knjige){
            if(knjiga.naziv == naziv){
                upisi = false;
            };
        };
        if(upisi == true){
            var knjiga = {
                "naziv": naziv,
                "autor": selectChooseAnAuthor,
                "zanr": zanr,
                "brStrana": parseInt(brStrana),
                "id": parseInt(id),
                "godina": parseInt(godIzdavanja),
                "obrisan": false,
                "cena": parseInt(cena),
                "kolicina": parseInt(brPrimeraka),
                "prica": prica,
                "slika": putanjaSlike
            };
            knjige.push(knjiga);
            window.localStorage.setItem("knjige", JSON.stringify(knjige));
            var imgtag = document.getElementById("imgSlika");
            imgtag.src = undefined;
        } else {
            window.alert("Knjiga sa unetim nazivom vec postoji. Pokusajte ponovo.")
        };
        $("#main")[0].reset();
        // console.log(knjiga);
    })(document);
};

function nKDodajSliku(event){
    var selectedFile = event.target.files[0];
    var reader = new FileReader();
  
    var imgtag = document.getElementById("imgSlika");
    imgtag.title = selectedFile.name;
  
    reader.onload = function(event) {
      imgtag.src = event.target.result;
      putanjaSlike = event.target.result;
    };

    reader.readAsDataURL(selectedFile);
}

function nKPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};