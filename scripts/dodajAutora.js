function dADodajAutora(){
    (function(d){
        var name = d.getElementById("username").value;
        var lastname = d.getElementById("lastname").value;

        if(JSON.parse(window.localStorage.getItem("autori")) == null){
            var autori = [];
            var autor = {
                "ime": name,
                "prezime": lastname
            };
            autori.push(autor);
            window.localStorage.setItem("autori", JSON.stringify(autori));
            window.alert("Autor dodat.");
        } else {
            var autori = JSON.parse(window.localStorage.getItem("autori"));
            var upisi = true;
            for(autor of autori){
                if(autor.ime == name && autor.prezime == lastname){
                    upisi = false;
                };
            };
            if(upisi){
                var autor = {
                    "ime": name,
                    "prezime": lastname
                };
                autori.push(autor);
                window.localStorage.setItem("autori", JSON.stringify(autori));
                window.alert("Autor dodat.");
            } else {
                window.alert("Autor sa unetim podacima postoji. Pokusajte ponovo.");
            };
        };
    })(document)
};

function dAPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};