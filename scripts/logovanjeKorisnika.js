function lKLogIn(){
    (function(d){
        var username = d.getElementById("username").value;
        var password = d.getElementById("password").value;

        // console.log(username, password);
        var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
        if(registrovaniKorisnici == null){
            window.alert("Korisnik sa unetim korisnickim imenom ne postoji. Pokusajte ponovo.");
        } else {
            var postoji = false;
            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == username && korisnik.uloga == "korisnik" && korisnik.sifra == password){
                    postoji = true;
                    trenutniKorisnik = korisnik;
                };
            };
            if(postoji){
                window.localStorage.setItem("trenutniKorisnik", JSON.stringify(trenutniKorisnik));
                window.location.replace("../templates/main.html");
            } else {
                window.alert("Korisnik sa unetim korisnickim imenom ne postoji. Pokusajte ponovo.");
            };
        };
    })(document);
};

function lKPocetna(){
    (function(d){
        window.location.replace("../templates/pocetna.html");
    })(document);
};